# Source
# http://www.catosplace.net/blog/2015/02/11/running-jenkins-in-docker-containers/

FROM jenkins
MAINTAINER Scott Doane <sdoane@austin.utexas.edu>

COPY plugins.txt /usr/share/jenkins/ref/
RUN /usr/local/bin/plugins.sh /usr/share/jenkins/ref/plugins.txt